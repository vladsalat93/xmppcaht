package org.coralmedia.simplexmppchat;

/**
 * Created by zbukervlad on 30.08.2016.
 */

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;
import android.app.AlarmManager;

import com.google.gson.Gson;

import java.util.Calendar;

import org.jivesoftware.smack.chat.Chat;

public class MyService extends Service {
    private static final String DOMAIN = "159.224.88.30";
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "secret";
    private static final String SERVICE_NAME = "localhost";
    public static ConnectivityManager cm;
    public static MyXMPP xmpp;
    public static boolean ServerchatCreated = false;
    String text = "";


    @Override
    public IBinder onBind(final Intent intent) {
        return new LocalBinder<MyService>(this);
    }

    public Chat chat;

    @Override
    public int onStartCommand(final Intent intent, final int flags,
                              final int startId) {
        cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        xmpp = MyXMPP.getInstance(MyService.this, DOMAIN, USERNAME, PASSWORD, SERVICE_NAME);
        xmpp.setMessageListener(new MMessageListener(MyService.this, new Gson()));
        xmpp.connect("onCreate");

        return Service.START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static boolean isNetworkConnected() {
        return cm.getActiveNetworkInfo() != null;
    }
}