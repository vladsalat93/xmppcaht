package org.coralmedia.simplexmppchat;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.gson.Gson;

import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;

/**
 * Created by zbukervlad on 30.08.2016.
 */
public class MMessageListener implements ChatMessageListener {
    private Context mContext;
    private Gson gson;

    private static final String TAG = "MMessageListener";

    public MMessageListener(Context contxt, Gson gson) {
        this.gson = gson;
        mContext = contxt;
    }

    @Override
    public void processMessage(final org.jivesoftware.smack.chat.Chat chat,
                               final Message message) {
        Log.i("MyXMPP_MESSAGE_LISTENER", "Xmpp message received: '"
                + message);

        if (message.getType() == Message.Type.chat
                && message.getBody() != null) {
            final ChatMessage chatMessage = gson.fromJson(
                    message.getBody(), ChatMessage.class);

            try{
                processMessage(chatMessage);
            }catch (Exception ignored){}


            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("message form: " + message.getFrom())
                    .setContentText(chatMessage.body)
                    .setAutoCancel(true);

            NotificationManager notificationManager =
                    (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

            Log.d(TAG, "recieved message");
        }
    }

    private void processMessage(final ChatMessage chatMessage) {

        chatMessage.isMine = false;
        Chats.chatlist.add(chatMessage);
        new Handler(Looper.getMainLooper()).post(new Runnable() {

            @Override
            public void run() {
                Chats.chatAdapter.notifyDataSetChanged();
            }
        });
    }
}
